public class Fridge
{
    public String color;
    public int voltage;
    public int weight;

    public void printColor (String color)
    {
        System.out.println("The fridge color is " + color);
    }

    public void printWeight (int weight)
    {
        System.out.println("The weight of the fridge is " + weight);
    }
}