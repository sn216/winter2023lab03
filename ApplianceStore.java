import java.util.Scanner;
public class ApplianceStore
{
    public static void main (String args[])
    {
        Scanner scan = new Scanner(System.in);
        Fridge[] appliance = new Fridge [4];
        


        for(int i = 0; i < 4; i++)
        {
            appliance[i] = new Fridge();

            System.out.println("Enter the fridge color");
            appliance[i].color = scan.next();

            System.out.println("Enter the fridge voltage");
            appliance[i].voltage = scan.nextInt();

            System.out.println("Enter the fridge weight");
            appliance[i].weight = scan.nextInt();
        }

        System.out.println("Color: " + appliance[3].color + " Voltage:" + appliance[3].voltage + " Weight: " + appliance[3].weight );

        appliance[0].printColor(appliance[0].color);
        appliance[0].printWeight(appliance[0].weight);
       
        scan.close();
    }
}
